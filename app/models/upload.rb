class Upload < ActiveRecord::Base
    mount_uploader :avatar, AvatarUploader
    
    validates :avatar, presence: true
end