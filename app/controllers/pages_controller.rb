require 'will_paginate/array'

class PagesController < ApplicationController
  before_action :set_account

  def index
    if !current_user.blank?
      @read_count = @gmail.inbox.emails(:read).count
      @unread_count = @gmail.inbox.emails(:unread).count
      @read = @gmail.inbox.emails(:read).reverse!.paginate(:page => params[:read_page], :per_page => 10)
      @unread = @gmail.inbox.emails(:unread).reverse!.paginate(:page => params[:unread_page], :per_page => 10)
    else
      render "login", :layout => "login_layout"
    end
  end

  def sent
    if !current_user.blank?
      @read_count = @gmail.mailbox('sent').emails(:read).count
      @unread_count = @gmail.mailbox('sent').emails(:unread).count
      @read = @gmail.mailbox('sent').emails(:read).reverse!.paginate(:page => params[:read_page], :per_page => 10)
      @unread = @gmail.mailbox('sent').emails(:unread).reverse!.paginate(:page => params[:unread_page], :per_page => 10)
    else
      render "login", :layout => "login_layout"
    end
  end

  def login
  end

  def new
  end

  def display_message
    @id = params[:id]
    @mailbox = params[:mailbox]
  end

  def send_message
    params = message_params
    if !params[:file].blank?
      u = Upload.create(avatar: params[:file])
      u.save!
      path = "#{Rails.root}" << "/public" << u.avatar.url
    end

    if (!(params[:cc].blank? && params[:bcc].blank? && params[:to].blank?))
      email = @gmail.compose do
        to "#{params[:to]}" unless params[:to].blank?
        cc "#{params[:cc]}" unless params[:cc].blank?
        bcc "#{params[:bcc]}" unless params[:bcc].blank?
        subject "#{params[:subject]}"
        text_part do
            body "#{params[:message]}"
        end
        html_part do
            body "#{params[:message]}"
        end
        add_file "#{path}" unless path.blank?
      end
      email.deliver!

      Upload.destroy_all
      redirect_to root_path, success: 'Message sent!'
    else
      redirect_to new_message_path, failure: "Error empty recipient."
    end
  end

  def message_params
    params.permit(:utf8, :authenticity_token, :commit, :to, :subject, :message, :file, :cc, :bcc)
  end

  def set_account
    unless current_user.blank?
      @gmail = Gmail.connect(:xoauth2, current_user.email, current_user.oauth_token)
    end
  end
end
