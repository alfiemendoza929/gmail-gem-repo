class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :current_user
  add_flash_types :success, :failure
  rescue_from Net::IMAP::BadResponseError, with: :logout

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id] rescue ""
  end

  def logout
    redirect_to signout_path, failure: "User account timed out. Log in again to continue.", success: ""
  end
end
